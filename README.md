![Demonstration](/reddit-search-selenium.gif)

# Reddit-search-selenium
This a selenium web scraper for scraping search result from Reddit and output
the result as JSON data.

# Run
- First install the requirements specified in *requirements.txt*
    - command `pip install -r requirements.txt`

- Then specify the search query in the *serach_query* variable in *main.py* file

- Execute the *main.py* file
    - command `python3 main.py`

# Doesn't Scroll
The scraper only scraps data for the first page and it doesn't scroll down
to load more results. Similarly it doesn't scroll down on post page to load
more comments.

# Top Level Comment
Only scrapes the top level comments, it doesn't scrape sub-comments(comment replies?).

# Some details
The scrapping work is done in a separate module named *redditsearch.py*, located at
*lib/redditsearch.py*. The *RDSearch* class takes the *serach_query* and it has a
property named *RDData* which is a **pydantic** Model.
