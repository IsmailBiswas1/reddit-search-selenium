from lib.redditsearch import RDSearch

search_query = "Best ever Linux distribution"
search = RDSearch(search_query)
file_path = f"{search_query}.json"
with open(file_path, "w") as json_file:
    json_file.write(search.rddata.model_dump_json())
