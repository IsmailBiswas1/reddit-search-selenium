from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ChromeOptions
from pydantic import BaseModel
from typing import List


class RDResults(BaseModel):
    title: str
    detail: str | None
    comments: List[str]


class RDData(BaseModel):
    query: str
    results: List[RDResults]


class RDSearch:
    def __init__(self, search_query: str) -> None:
        options = ChromeOptions()
        options.add_argument("--headless=new")
        options.add_argument(
            "--user-agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'"
        )
        self.__driver: webdriver.Chrome = webdriver.Chrome(options=options)
        self.__domain: str = "https://www.reddit.com"
        self.__results: List[RDResults]
        self.__driver.implicitly_wait(1)
        self.search_query: str = search_query
        self.search_url = f"{self.__domain}/search/?q={search_query}"
        self.__retrive_post_data(self.search_url)

    def __get_detail(self) -> str | None:
        """
        Returns the post's description if the description exists else
        return None
        """

        # Try to get the detail of the post and if no description exists handle it gracefully.
        try:
            detail = self.__driver.find_element(
                By.XPATH,
                "/html/body/shreddit-app/dsa-transparency-modal-provider/div/main/shreddit-post/div[2]/div/div/p",
            )
            return detail.text
        except NoSuchElementException as _:
            print("No description for this post")

    def __get_comments(self) -> list[str]:
        """Returns first page's top level comments (Doesn't scroll down)"""

        comment_content = []

        comments = self.__driver.find_elements(
            By.XPATH, "//shreddit-comment-tree/shreddit-comment/div[3]/div"
        )

        for comment in comments:
            comment_content.append(comment.text)
        return comment_content

    def __retrive_post_data(self, search_url: str) -> None:
        """
        Loops through the first page's results (Doesn't scroll down) to
        collect and set data to the RDData model.
        """
        self.__driver.get(search_url)
        results = []

        search_items = self.__driver.execute_script(
            'return document.querySelector("dsa-transparency-modal-provider > search-dynamic-id-cache-controller").querySelectorAll("reddit-feed > faceplate-tracker")'
        )

        for index in range(len(search_items)):
            search_items = self.__driver.execute_script(
                'return document.querySelector("dsa-transparency-modal-provider > search-dynamic-id-cache-controller").querySelectorAll("reddit-feed > faceplate-tracker")'
            )

            title_link = search_items[index].find_element(By.TAG_NAME, "a")
            title_span = title_link.find_element(By.TAG_NAME, "span")
            title = self.__driver.execute_script(
                "return arguments[0].textContent;", title_span
            )

            # Extract the link to the post's detail page
            link_value = self.__driver.execute_script(
                "return arguments[0].getAttribute('href');", title_link
            )

            # Navigate the post's detail page.
            self.__driver.get(f"{self.__domain}{link_value}")

            post_detail = self.__get_detail()
            post_comments = self.__get_comments()
            self.__driver.back()

            results.append(
                RDResults(title=title, detail=post_detail, comments=post_comments)
            )

        self.__results = results
        self.rddata: RDData = RDData(query=self.search_query, results=self.__results)
        self.__driver.quit()
